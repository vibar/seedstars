# PHP Engineer Challenge #

Install dependencies

```
#!bash

composer install
```

Run migrations

```
#!bash

php artisan migrate
```

(Optional) Config ```jenkins_url``` in ```config/app.php``` (default http://localhost:8080)

** Console Script **

```
#!bash

php artisan jenkins:update
```

** Full App **

http://localhost