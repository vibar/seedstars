<?php

namespace App\Console\Commands;

use App\Job;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateJenkins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jenkins:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update jobs status from Jenkins';

    /**
     * @var Job
     */
    private $job;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Url
     */
    private $url;

    /**
     * Create a new command instance.
     *
     * @param Job $job
     * @param Client $client
     */
    public function __construct(Job $job, Client $client)
    {
        parent::__construct();

        $this->url = config('app.jenkins_url') . '/api/json?tree=jobs[name,builds[result,number]]';
        $this->job = $job;
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->client->get($this->url);

        $jobs = $response->json()['jobs'];

        foreach ($jobs as $j) {

            $j = (object) $j;

            $this->job = $this->job->firstOrNew(['name' => $j->name]);

            if (! $j->builds) continue;

            $last = (object) $j->builds[0];

            $this->job->name = $j->name;
            $this->job->number = $last->number;
            $this->job->status = $last->result;

            $this->job->save();

        }
    }
}
