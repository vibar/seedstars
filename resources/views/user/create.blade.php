@extends('layouts.master')

@section('content')

    @if (count($errors))

        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>

    @endif

    {!! Form::open(['route' => 'user.store' ]) !!}

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', '', ['class' => 'form-control']) !!}
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', '', ['class' => 'form-control']) !!}
            </div>
        </div>

    </div>

    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}

@endsection