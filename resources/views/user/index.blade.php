@extends('layouts.master')

@section('content')

    <div class="row table-responsive">
        
        <div class="col-md-12">
            
            <table class="table table-striped">
                
                <thead>
                
                    <th>Name</th>
                    <th>Email</th>

                </thead>

                <tbody>

                    @foreach($users as $u)

                        <tr>

                            <td>{{ $u->name }}</td>
                            <td>{{ $u->email }}</td>
                            
                        </tr>

                    @endforeach

                </tbody>

            </table>

        </div>

    </div>

    {!! Form::close() !!}

@endsection